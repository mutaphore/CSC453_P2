#Compile the code

program_2: program_2.c os.c serial.c os.h globals.h
	avr-gcc -mmcu=atmega328p -DF_CPU=16000000 -O2 -o pg2.elf program_2.c os.c serial.c
	avr-objcopy -O ihex pg2.elf pg2.hex
	avr-size pg2.elf

#Flash the Arduino
#Be sure to change the device (the argument after -P) to match the device on your computer
#On Windows, change the argument after -P to appropriate COM port
program: pg2.hex
	avrdude -pm328p -P /dev/tty.usbmodem1411 -c arduino -F -u -U flash:w:pg2.hex

#remove build files
clean:
	rm -fr *.elf *.hex *.o
