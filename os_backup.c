#include "globals.h"
#include "os.h"

volatile system_t sysInfo;   //Global holding system information
register uint8_t debug asm("r6");

uint8_t get_next_thread(void) {
   return (sysInfo.curId + 1) % sysInfo.numThreads;
}

//This interrupt routine is automatically run every 10 milliseconds
ISR(TIMER0_COMPA_vect) {
   volatile uint8_t oldId;

   sysInfo.runtime = sysInfo.numIntr / 100;
   sysInfo.intrSec = sysInfo.numIntr++ / sysInfo.runtime;

   //The following statement tells GCC that it can use registers r18-r27,
   //and r30-31 for this interrupt routine.  These registers (along with
   //r0 and r1) will automatically be pushed and popped by this interrupt routine.
   asm volatile ("" : : : "r18", "r19", "r20", "r21", "r22", "r23", "r24", \
                 "r25", "r26", "r27", "r30", "r31");

   //Call get_next_thread to get the thread id of the next thread to run
   oldId = sysInfo.curId;
   sysInfo.curId = get_next_thread();
   context_switch(&sysInfo.threads[sysInfo.curId].tp,
    &sysInfo.threads[oldId].tp);

}

//Call this to start the system timer interrupt
void start_system_timer() {
   TIMSK0 |= _BV(OCIE0A);  //interrupt on compare match
   TCCR0A |= _BV(WGM01);   //clear timer on compare match

   //Generate timer interrupt every ~10 milliseconds
   TCCR0B |= _BV(CS02) | _BV(CS00);    //prescalar /1024
   OCR0A = 156;             //generate interrupt every 9.98 milliseconds
}

//new_tp: r25:24, old_tp: r23:r22
__attribute__((naked)) void context_switch(uint16_t* new_tp, uint16_t* old_tp) {

   //Save registers in regs_context_switch to stack
   asm volatile ("PUSH r2\n"
                 "PUSH r3\n"
                 "PUSH r4\n"
                 "PUSH r5\n"
                 "PUSH r6\n"
                 "PUSH r7\n"
                 "PUSH r8\n"
                 "PUSH r9\n"
                 "PUSH r10\n"
                 "PUSH r11\n"
                 "PUSH r12\n"
                 "PUSH r13\n"
                 "PUSH r14\n"
                 "PUSH r15\n"
                 "PUSH r16\n"
                 "PUSH r17\n"
                 "PUSH r28\n"
                 "PUSH r29");

   //Store old SP address to old_tp
   asm volatile ("CLR r31\n");
   asm volatile ("LDI r30, 0x5D\n");
   asm volatile ("LD r16, Z\n");
   asm volatile ("CLR r31\n");
   asm volatile ("LDI r30, 0x5E\n");
   asm volatile ("LD r17, Z\n");
   asm volatile ("MOV r30, r22\n");
   asm volatile ("MOV r31, r23\n");
   asm volatile ("ST Z+, r16\n");   //Low byte
   asm volatile ("ST Z, r17");      //High byte

   //Load new SP address from new_tp
   asm volatile ("MOV r30, r24\n");
   asm volatile ("MOV r31, r25\n");
   asm volatile ("LD r16, Z+\n");   //Low byte
   asm volatile ("LD r17, Z\n");    //High byte
   asm volatile ("CLR r31\n");
   asm volatile ("LDI r30, 0x5D\n");
   asm volatile ("ST Z, r16\n");
   asm volatile ("CLR r31\n");
   asm volatile ("LDI r30, 0x5E");
   asm volatile ("ST Z, r17\n");

   //Load registers from stack
   asm volatile ("POP r29\n"
                 "POP r28\n"
                 "POP r17\n"
                 "POP r16\n"
                 "POP r15\n"
                 "POP r14\n"
                 "POP r13\n"
                 "POP r12\n"
                 "POP r11\n"
                 "POP r10\n"
                 "POP r9\n"
                 "POP r8\n"
                 "POP r7\n"
                 "POP r6\n"
                 "POP r5\n"
                 "POP r4\n"
                 "POP r3\n"
                 "POP r2");

   //return PC
   asm volatile ("RET");

}

__attribute__((naked)) void thread_start(void) {
   //enable interrupts
   sei();

   //Load args from r5:r4 to r25:r24
   asm volatile ("MOV r24, r4");
   asm volatile ("MOV r25, r5");

   //Use r2:r3 for function address
   asm volatile ("MOV r30, r2");   //Copy function address to Z
   asm volatile ("MOV r31, r3");
   asm volatile ("IJMP");           //Jump to function
}

void os_start(void) {
   regs_context_switch dummy;   //dummy storage for the initial pushes

   start_system_timer();
   context_switch(&sysInfo.threads[0].tp, &dummy);
}

void os_init() {

   sysInfo.runtime = 0;
   sysInfo.intrSec = 0;
   sysInfo.numThreads = 0;
   sysInfo.numIntr = 0;
   sysInfo.curId = 0;         //Start with thread 0
}

void create_thread(uint16_t address, void *args, uint16_t stack_size) {
   uint16_t totSize;
   uint8_t *stackEnd, id;

   id = sysInfo.numThreads++;   //Increment numThreads;
   totSize = sysInfo.threads[id].totSize =
    stack_size + sizeof(regs_context_switch) + sizeof(regs_interrupt) + 32;

   //Set thread info
   sysInfo.threads[id].id = id;
   sysInfo.threads[id].stackBase = calloc(1, totSize);
   sysInfo.threads[id].userSize = stack_size;
   sysInfo.threads[id].pc = address;

   stackEnd = sysInfo.threads[id].stackEnd
    = sysInfo.threads[id].stackBase + totSize;

   //Put thread_start function address in PC
   *(stackEnd - 1) = (uint8_t)(thread_start);               //low PC byte
   *(stackEnd - 2) = (uint8_t)((uint16_t)thread_start >> 8);//high PC byte

   //Put function address in r3:r2
   *(stackEnd - 3) = (uint8_t)(address);        //low addr byte r2
   *(stackEnd - 4) = (uint8_t)(address >> 8);   //high addr byte r3

   if (args != NULL) {
      //Put function args in r5:r4
      *(stackEnd - 5) = (uint8_t)args;                   //low arg byte r4
      *(stackEnd - 6) = (uint8_t)((uint16_t)args >> 8);  //high arg byte r5
   }

   //Store stack pointer
   sysInfo.threads[id].tp =
    (uint16_t)(stackEnd - sizeof(regs_context_switch));

}
