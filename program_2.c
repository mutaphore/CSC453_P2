#include "os.h"
#include "globals.h"

void write_int(uint32_t num) {
   uint8_t iVal[MAX_DIGITS], ndx = MAX_DIGITS;

   do {
      iVal[--ndx] = '0' + num % 10;
      num /= 10;
   } while (num > 0);

   while (ndx < MAX_DIGITS)
      write_byte(iVal[ndx++]);
}

void led_on() {
   //Set DDRB bit 5 to 1
   asm volatile ("LDI r30, 0x24");
   asm volatile ("LD r29, Z");
   asm volatile ("ORI r29, 0x20");
   asm volatile ("ST Z, r29");
   //Set PORTB bit 5 to 1
   asm volatile ("LDI r30, 0x25");
   asm volatile ("LD r29, Z");
   asm volatile ("ORI r29, 0x20");
   asm volatile ("ST Z, r29");
}

void led_off() {
   //Set DDRB bit 5 to 1
   asm volatile ("LDI r30, 0x24");
   asm volatile ("LD r29, Z");
   asm volatile ("ORI r29, 0x20");
   asm volatile ("ST Z, r29");
   //Set PORTB bit 5 to 0
   asm volatile ("LDI r30, 0x25");
   asm volatile ("LD r29, Z");
   asm volatile ("ANDI r29, 0xDF");
   asm volatile ("ST Z, r29");
}

void blink(uint16_t *delay) {
   uint16_t i, d = *delay/10;

   while (1) {
      led_on();
      for(i = 0; i < d; i++)
         _delay_ms(10);
      led_off();
      for(i = 0; i < d; i++)
         _delay_ms(10);
   }
}

void stats() {
   uint8_t i, input;

   clear_screen();

   while (1) {
      //Change color

      write_byte(ESC);
      write_byte('[');
      write_int(YELLOW);
      write_byte('m');
      print_string("System time (s): ");
      print_int32(sysInfo.runtime);
      next_line();
      print_string("Interrupts/second: ");
      print_int32(sysInfo.intrSec);
      next_line();
      print_string("Number of Threads: ");
      print_int(sysInfo.numThreads);
      next_line();
      for (i = 0; i < sysInfo.numThreads; i++) {
         write_byte(ESC);
         write_byte('[');
         write_int(YELLOW);
         write_byte('m');
         print_string("---------------------");
         next_line();
         write_byte(ESC);
         write_byte('[');
         write_int(GREEN);
         write_byte('m');
         print_string("Thread id: ");
         print_int(sysInfo.threads[i].id);
         next_line();
         print_string("Thread PC: ");
         print_hex(sysInfo.threads[i].pc);
         next_line();
         print_string("Stack usage (bytes): ");
         print_int(
          (uint16_t)sysInfo.threads[i].stackEnd - sysInfo.threads[i].tp);
         next_line();
         print_string("Stack size (bytes): ");
         print_int(sysInfo.threads[i].totSize);
         next_line();
         print_string("Top of stack: ");
         print_hex(sysInfo.threads[i].tp);
         next_line();
         print_string("Stack base: ");
         print_hex(sysInfo.threads[i].stackBase);
         next_line();
         print_string("Stack end: ");
         print_hex(sysInfo.threads[i].stackEnd);
         next_line();
      }
      set_cursor(0, 0);
   }

}

int main() {
   uint16_t delay = 500;   //Argument for blink()

   serial_init();
   os_init();
   create_thread(blink, &delay, 32);
   create_thread(stats, NULL, 32);
   os_start();

   return 0;
}
